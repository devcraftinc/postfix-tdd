package postfix;

import static java.lang.Character.isDigit;
import static java.lang.Double.parseDouble;

import java.util.Stack;

public class PostfixCalculator {

	private int position = 0;

	public double eval(String expression) {
		position = 0;
		Stack<Double> operands = new Stack<>();
		String token = nextToken(expression);
		while (token != null) {
			if (isOperand(token)) {
				double operand = parseDouble(token);
				operands.push(operand);
			} else {
				applyOperator(operands, token);
			}
			token = nextToken(expression);
		}
		return operands.peek();
	}

	private void applyOperator(Stack<Double> operands, String token) {
		if ("+".equals(token)) {
			operands.push(operands.pop() + operands.pop());
		} else if ("-".equals(token)) {
			operands.push(-operands.pop() + operands.pop());
		}
	}

	private boolean isOperand(String token) {
		return Character.isDigit(token.charAt(0));
	}

	private String nextToken(String expression) {
		StringBuilder tokenBuff = new StringBuilder();
		for (; position < expression.length() && isDigit(expression.charAt(position)); position++)
			tokenBuff.append(expression.charAt(position));

		if (tokenBuff.length() > 0)
			return tokenBuff.toString();

		if (position < expression.length())
			return "" + expression.charAt(position++);

		return null;
	}

}
