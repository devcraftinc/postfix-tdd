package postfix;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PostfixTest {

	PostfixCalculator calculator = new PostfixCalculator();
	
	@Test
	public void oneOperandExpression() throws Exception {
		assertEquals(0.0, calculator.eval("0"), 0);
		assertEquals(10.0, calculator.eval("10"), 0);
	}
	
	@Test
	public void twoOperandsExpression() throws Exception {
		assertEquals(3.0, calculator.eval("1,2+"), 0);
		assertEquals(1.0, calculator.eval("3,2-"), 0);
	}
}